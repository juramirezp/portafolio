const articulos = document.getElementById('articles');
const loader = document.getElementById('loader');
var db = firebase.firestore();

db.collection("articles").get().then(function(querySnapshot) {
    querySnapshot.forEach(function(doc) {

        var time = moment(doc.data().date.seconds * 1000).format("DD-MM-YYYY");

        articles.innerHTML += `
        <div class="card">
            <div class="card__content">

                <div class="card__title">
                    ${doc.data().title}
                </div>

                <div class="card__line"></div>

                <ul class="card__list">
                    <li class="card__list__item">
                        <span>Autor:</span>${doc.data().author}
                    </li>
                    <li class="card__list__item">
                        <span>Fuente:</span>${doc.data().site}
                    </li>
                    <li class="card__list__item">
                        <span>Fecha:</span>${time}
                    </li>
                </ul>

                <ul class="card__buttons card__butttons--articles">
                    <div class="card__button">
                        <a href="${doc.data().url}" target="_blank">Ir al articulo</a>
                    </div>
                </ul>

            </div>
        </div>
        `
    });

    loader.classList.add('d-none');
    articulos.classList.remove('d-none');
    articulos.classList.add('fade-in');
});