const books = document.getElementById('books');
var db = firebase.firestore();

db.collection("books").get().then(function(querySnapshot) {
    querySnapshot.forEach(function(doc) {

        books.innerHTML += `
            <div class="card">

                <img src="${doc.data().image}" alt="${doc.data().title}" class="card__image card__image--book">

                
                <div class="card__content">

                    <div class="card__title">
                        ${doc.data().title}
                    </div>
2
                    <div class="card__subtitle">
                        ${doc.data().author}
                    </div>

                    <div class="card__line"></div>
    
                    <div class="card__paragraph">
                        ${doc.data().description}
                    </div>

                </div>
            </div>
        `
    });

    loader.classList.add('d-none');
    books.classList.remove('d-none');
    books.classList.add('fade-in');
});